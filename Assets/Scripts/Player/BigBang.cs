﻿using UnityEngine;
using System.Collections;

public class BigBang : MonoBehaviour {


	// Use this for initialization
	void Start () {
		GameObject[] destroyables = GameObject.FindGameObjectsWithTag("Destroyable");
		Debug.Log ("Asteroids found" + destroyables.Length);
		foreach(var destroyable in destroyables){
			destroyable.GetComponent<LifeControl>().TakeDamage(100f);
		}

	}
	

}
