﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour {

	public GameObject bulletPrefab;

	public float shotInterval = 0.5f;
	public float bulletSpeed = 50f;

	public AudioSource shootSound;

	private float cooldown = 0f;

	void Start(){
		shootSound = GetComponent<AudioSource>();
	}

	void Update(){
		if (cooldown == 0) {
			if(Input.GetButton("Fire1")){
				Shot ();
			}
		} 
		else{
			cooldown-=Mathf.Min(Time.deltaTime,cooldown);
		}
	}

	void Shot(){
		var bullet = GameObject.Instantiate (bulletPrefab, transform.position, Quaternion.LookRotation(transform.forward, transform.up)) as GameObject;
		bullet.rigidbody2D.velocity = transform.up * bulletSpeed;
		cooldown = shotInterval;
		shootSound.pitch = Random.Range(0.7f,1.2f);
		shootSound.Play();
	}
}
