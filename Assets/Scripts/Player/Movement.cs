﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (LifeControl))]
public class Movement : MonoBehaviour {
	
	public float accRate = 150;
	[Range(0,1)]
	public float deccelRate = 0.2f;

	public float rotationSpeed = 5f;

	private Animator animator;
	private LifeControl lifeControl;

	void Start(){
		animator = GetComponent<Animator>();
		lifeControl = GetComponent<LifeControl>();
	}

	// Update is called once per frame
	void Update () {
		float accel = Input.GetAxis ("Vertical") * accRate;
		animator.SetFloat("Acceleration", accel);
		if(Input.GetKey(KeyCode.Space)){
			rigidbody2D.velocity = Vector2.Lerp(rigidbody2D.velocity,Vector2.zero,deccelRate);
		}
		
		rigidbody2D.AddRelativeForce (Vector2.up * accel*Time.deltaTime);
		
		rigidbody2D.angularVelocity = -Input.GetAxis ("Horizontal")*rotationSpeed;
	}

	void OnCollisionEnter2D(Collision2D col){
		var other = col.gameObject;
		lifeControl.TakeDamage (10f);
		Vector2 forceDirection = (transform.position - other.transform.position);
		forceDirection.Normalize ();
		rigidbody2D.AddForce (forceDirection*200f);
	}

	void TakeDamage(float dmgAmount){
		animator.SetTrigger("Damaged");
	}
}

