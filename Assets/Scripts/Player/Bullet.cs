﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float damage = 20;

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Destroyable") {
			other.GetComponent<LifeControl>().TakeDamage(damage);
		}

		GameObject.Destroy (gameObject);
	}

}
