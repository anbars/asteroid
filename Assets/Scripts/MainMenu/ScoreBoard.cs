﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;


public class ScoreBoard : MonoBehaviour {

	public Text board;
	public ScoreManager scoreManager;

	private Dictionary<string, float> scoresTable;


	// Use this for initialization
	void Start () {
		scoresTable = scoreManager.GetTable();
		board.text = "";
		BuildBoard();
	}
	

	void BuildBoard(){
		var names = scoresTable.Keys.ToArray();
		if (names.Length>0) {
			//board.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, board.font.fontSize*10*names.Length);
			int count = 1;
			foreach (var name in names.OrderByDescending(name => scoresTable[name])) {
				board.text += count.ToString () + ".\t" + name + "\t" + scoresTable [name].ToString () + "\n";
				count++;
			}
		}
		else{
			board.rectTransform.sizeDelta = new Vector2 (board.rectTransform.rect.width, 50);
			board.text += "No Highscore yet. Yours will be first!";
		}
	}
}
