﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public GameObject exitConfirmPrefab;
	public Canvas mainCanvas;
	public Animator menuAnimator;


	// Use this for initialization
	void Start () {
		Time.timeScale = 1f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void StartGame(){
		Application.LoadLevel("GameScene");
	}

	public void Scores(){
		menuAnimator.SetTrigger("Scores");
	}

	public void ExitGame(){
		var confirm = Instantiate(exitConfirmPrefab) as GameObject;
		confirm.transform.SetParent(mainCanvas.transform, false);
	}
}
