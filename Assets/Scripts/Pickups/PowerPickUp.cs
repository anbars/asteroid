﻿using UnityEngine;
using System.Collections;

public class PowerPickUp : PickUp {

	public float powerShotInterval;
	public float duration;
	public GameObject gunsPrefub;


	private Shooting[] weapons;

	private GameObject[] newGuns;
	private float baseShotInterval;

	// Use this for initialization
	void Start () {
		newGuns = new GameObject[2];
		action = Boost;
		isContinious = true;
	}

	void Boost(){
		weapons = playerShip.GetComponentsInChildren<Shooting>() as Shooting[];
		baseShotInterval = weapons[0].shotInterval;

		for(int i = 0; i <2; i++){
			newGuns[i] = GameObject.Instantiate(gunsPrefub, playerShip.transform.position, Quaternion.identity) as GameObject;
			newGuns[i].transform.SetParent(playerShip.transform, true);
			newGuns[i].transform.rotation = playerShip.transform.rotation;
			Vector3 gunPosition = new Vector3 (0.4f, 0.4f, 0);
			if(i==1){
				gunPosition.x *=-1;
			}
			newGuns[i].transform.localPosition = gunPosition;
			var weapon = newGuns[i].GetComponent<Shooting>();
			weapon.shotInterval = powerShotInterval;
		}

		foreach(var weapon in weapons){
			weapon.shotInterval = powerShotInterval;
		}
		Invoke("Revert", duration);
	}

	void Revert(){
		foreach(var gun in newGuns){
			Destroy(gun);
		}
		foreach(var weapon in weapons){
			weapon.shotInterval = baseShotInterval;
		}

		Destroy(gameObject);
	}
}
