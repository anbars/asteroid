﻿using UnityEngine;
using System.Collections;

public class ScorePickUp : PickUp {
	
	private ScoreManager scoreManager;
	
	// Use this for initialization
	void Start () {
		scoreManager = GameObject.FindWithTag("ScoreManager").GetComponent<ScoreManager>();
		action = () => scoreManager.Add(5000f);
	}
}
