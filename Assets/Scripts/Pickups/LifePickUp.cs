﻿using UnityEngine;
using System.Collections;

public class LifePickUp : PickUp {

	private playerInstantiate instScript;

	// Use this for initialization
	void Start () {
		var playerLifeControl = GameObject.FindGameObjectWithTag ("Player").GetComponent<LifeControl> ();
		action = () => playerLifeControl.RestoreLife (100f);
	}
	
}
