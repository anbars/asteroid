﻿using UnityEngine;
using System.Collections;

public class BombPickUp : PickUp {

	private BigBangControl bombControl;

	// Use this for initialization
	void Start () {
		bombControl = GameObject.FindWithTag("GM").GetComponent<BigBangControl>();
		action = () => bombControl.Add(1);
	}
	
}
