﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public abstract class PickUp : MonoBehaviour {
		
	protected delegate void Action();
	protected Action action;
	protected GameObject playerShip;

	protected bool isContinious = false;



	virtual protected void OnTriggerEnter2D(Collider2D coll){
		if(coll.gameObject.CompareTag("Player")){
			audio.Play();
			playerShip = coll.gameObject;
			Debug.Log("Hitted " + playerShip.tag);
			if(action != null){
				action();
			}
			GetComponent<SpriteRenderer>().enabled = false;
			collider2D.enabled = false;
			if (!isContinious) {
				var destroy = gameObject.AddComponent<DestroyOnDelay>();
				destroy.delayTime = 2.5f;

			}

		}
	}
}
