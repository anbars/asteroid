﻿using UnityEngine;
using System.Collections;

public class BigBangControl : MonoBehaviour {

	public int startBombs = 5;
	public Transform explosionPoint;
	public GameObject explosionPrefab;
	public BombPanelHandler bombPanel;

	private int curBombs = 0;

	// Use this for initialization
	void Start () {
		curBombs = startBombs;
	}
	
	// Update is called once per frame
	void Update () {
		if(curBombs != 0 && Input.GetButtonDown("Fire2")){
			Instantiate(explosionPrefab, explosionPoint.position, Quaternion.identity);
			curBombs--;
			bombPanel.RemoveIcon();
		}
	}

	public void Add (int num){
		int bombsToAdd = num;

		if(bombsToAdd != 0){
			curBombs += bombsToAdd;
			for(int i = 0; i< bombsToAdd; i++){
				bombPanel.AddIcon();
			}
		}
	}
}
