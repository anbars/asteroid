﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class ScoreManager : MonoBehaviour {

	public CounterWithDelay scoreCounter;

	public float Score{get; private set;}



	private Dictionary<string, float> scoresTable;

	void Awake(){
		InitializeTable(out scoresTable);
	}

	public void Add(float delta){
		Score += delta;
		updateScoreCounter();
	}

	void updateScoreCounter(){
		scoreCounter.SetValue(Score);
	}

	void InitializeTable(out Dictionary<string, float> table){
		try{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(Application.persistentDataPath + "/Scores.dat", FileMode.Open, FileAccess.Read, FileShare.None);
			table = formatter.Deserialize(stream) as Dictionary<string, float>;
			stream.Close();
		} catch(FileNotFoundException excep){
			table = new Dictionary<string, float>();
		}
	}

	public void SaveScore(string playerName){
		if(scoresTable.ContainsKey(playerName)){
			scoresTable[playerName] = Score;
		}
		else{
			scoresTable.Add(playerName, Score);
		}
		SaveTable(scoresTable);
	}

	void SaveTable(Dictionary<string, float> table){
		IFormatter formatter = new BinaryFormatter();
		Stream stream = new FileStream(Application.persistentDataPath + "/Scores.dat", FileMode.Create, FileAccess.Write, FileShare.None);
		formatter.Serialize(stream, table);
		stream.Close();
	}

	public Dictionary<string, float> GetTable(){
		return scoresTable;
	}
	
}
