﻿using UnityEngine;
using System.Collections;

public class LifeControl : MonoBehaviour {

	[Header("Health")]
	public float lifeTotal = 100;

	public GameObject afterDestroyObject;

	[Header("Shields")]
	public bool hasShields = false;
	public float shieldHealthTotal = 30f;
	public float regenDelay = 10f;
	public float regenRate = 5f;

	public float ShieldHelthCur {get; protected set;}


	public delegate void DestroyEventHandler();

	public delegate void HealthChangeHamdler(float delta, float current);

	public event DestroyEventHandler onDestroy;

	public event HealthChangeHamdler onLifeChange;
	public event HealthChangeHamdler onShieldChange;

	public float LifeCurrent{get; protected set;}

	private float shieldsCooldown = 0f;

	protected Animator animator;

	void Start(){
		LifeCurrent = lifeTotal;
		ShieldHelthCur = shieldHealthTotal;
		animator = GetComponent<Animator>();
	}

	void Update(){
		if(hasShields){
			if(shieldsCooldown > 0.001f){
				shieldsCooldown -= Time.deltaTime;
			}
			else if (ShieldHelthCur < shieldHealthTotal){
				float shieldHeal = Mathf.Min (regenRate * Time.deltaTime, shieldHealthTotal - ShieldHelthCur);
				ShieldHelthCur += shieldHeal;
				if(onShieldChange != null) {
					onShieldChange(shieldHeal, ShieldHelthCur);
				}
			}
		}
	}

	public void TakeDamage(float damageTaken){

		if (hasShields) {
			shieldsCooldown = regenDelay;
		}
		
		if(hasShields && ShieldHelthCur > 0.001f){
			float shieldDamage = Mathf.Min(damageTaken, ShieldHelthCur);
			ShieldHelthCur -= shieldDamage;

			if (onShieldChange != null) {
				onShieldChange(shieldDamage,ShieldHelthCur);
			}

			damageTaken -= shieldDamage;
			}

		if (damageTaken > 0.001f) {
			float healthDamage = Mathf.Min (LifeCurrent, damageTaken);
			LifeCurrent -= healthDamage;
			
			if (onLifeChange != null) {
				onLifeChange (healthDamage, LifeCurrent);
			}
			
			if (LifeCurrent <= 0) {
				Destroyed ();
			}
			
			if (animator) {
				animator.SetTrigger ("Damaged");
			}
		}
	}

	public void RestoreLife(float amount){
		float lifeToRestore = Mathf.Min (lifeTotal - LifeCurrent, amount);
		Debug.Log ("LifeToRestore: " + lifeToRestore);
		if (lifeToRestore <= 0) {
			return;		
		}
		LifeCurrent += lifeToRestore;

		if (onLifeChange != null) {
			onLifeChange (lifeToRestore, LifeCurrent);
		}
	}

	void Destroyed(){

		if(afterDestroyObject){
			GameObject.Instantiate(afterDestroyObject, transform.position, Quaternion.identity);
		}

		if(onDestroy != null){
			onDestroy();
		}

		Destroy(gameObject);
	}

}
