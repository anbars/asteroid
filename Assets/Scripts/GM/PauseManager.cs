﻿using UnityEngine;
using System.Collections;

public class PauseManager : MonoBehaviour {

	private float storedScale;
	public GameObject pauseMenu;
	public GameObject confirmPrefab;
	public Canvas canvas;

	private Animator animator;

	private bool paused;
	private bool confirmSet;

	// Use this for initialization
	void Start () {
		animator = pauseMenu.GetComponent<Animator>();
		storedScale = Time.timeScale;
		paused = false;
		confirmSet = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape) && !confirmSet){
			if (!paused) {
				Pause ();
			}
			else{
				Resume();
			}
		}
	}

	public void Pause(){
		storedScale = Time.timeScale;
		Time.timeScale = 0;
		animator.SetTrigger("Paused");
		paused = true;
		Screen.showCursor = true;
	}

	public void Resume(){
		Time.timeScale = storedScale;
		animator.SetTrigger("Resumed");
		paused = false;
		Screen.showCursor = false;
	}

	public void Restart(){
		ConfirmWindow.Action onConfirm = ()=> Application.LoadLevel("GameScene");
		CreateConfirmWindow(onConfirm);
	}

	public void GiveUp(){
		ConfirmWindow.Action onConfirm = ()=> GetComponent<LevelControl>().GameOver();
		CreateConfirmWindow(onConfirm);
	}

	public void Exit(){
		ConfirmWindow.Action onConfirm = ()=> Application.Quit();
		CreateConfirmWindow(onConfirm);
	}

	void CreateConfirmWindow(ConfirmWindow.Action onConfirm){
		var confirmWindow = Instantiate(confirmPrefab) as GameObject;
		confirmWindow.transform.SetParent(canvas.transform, false);

		confirmSet = true;

		var confirmScript = confirmWindow.GetComponent<ConfirmWindow>();

		animator.SetTrigger("Resumed");

		ConfirmWindow.Action onCancel = ()=> {
			animator.SetTrigger("Paused");
			confirmSet = false;
		};
		confirmScript.SetAction(onConfirm, onCancel);
	}
}
