﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreSaveWindow : MonoBehaviour {

	public Button okButton;
	public Text scoreText;

	private ScoreManager scoreManager;

	private string playerName;

	// Use this for initialization
	void Start () {
		scoreManager = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
		scoreText.text = "Your Score is\n" + scoreManager.Score.ToString();
	}
	


	public void SetName(string name){
		playerName = name;
		okButton.interactable = playerName != "";
	}

	public void Save(){
		scoreManager.SaveScore(playerName);
		Application.LoadLevel("MainMenu");
	}

	public void Cancel(){
		Application.LoadLevel("MainMenu");
	}


}
