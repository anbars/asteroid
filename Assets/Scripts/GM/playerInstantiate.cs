﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class playerInstantiate : MonoBehaviour {

	public Transform spawnPoint;
	public GameObject playerPrefab;

	public float instantiateDelay = 3f;

	public PopUpControl popUp;

	public delegate void InstantiateEventHandler(GameObject newPlayerShip);

	public event InstantiateEventHandler onInstatiate;


	public int startLifes = 5;
	public LifePanelHandler lifePanel;

	private GameObject playerShip;
	private LifeControl playerLifeControl;

	public int lifesLeft {get; private set;}

	// Use this for initialization
	void Start () {
		InstatiatePlayer();
		lifesLeft = startLifes;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void InstatiatePlayer(){
		playerShip = GameObject.Instantiate(playerPrefab, spawnPoint.position, Quaternion.identity) as GameObject;
		playerLifeControl = playerShip.GetComponent<LifeControl>();
		playerLifeControl.onDestroy+= handlePlayerDeath;

		if(onInstatiate != null){
			onInstatiate(playerShip);
		}
	}

	void handlePlayerDeath(){
		lifePanel.RemoveIcon();
		lifesLeft--;
		if (lifesLeft != 0) {
			popUp.SetCountdown(instantiateDelay, "You Died!\nRespawn in", InstatiatePlayer);
		}
		else{
			GetComponent<LevelControl>().GameOver();
		}
	}

	public void AddLifes(int num){
		int lifesToAdd = num;
		
		if(lifesToAdd != 0){
			lifesLeft += lifesToAdd;
			for(int i = 0; i< lifesToAdd; i++){
				lifePanel.AddIcon();
			}
		}
	}
}
