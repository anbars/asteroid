﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(playerInstantiate))]
public class LevelControl : MonoBehaviour {
	public float nextStageDelay = 5f;

	[Header("Scores")]
	public ScoreManager scoreManager;
	public float scorePerAsteroid = 100f;
	public float scorePerLife = 500f;

	[Header("Asteroid Control")]
	public GameObject asteroidPrefab;
	public int maxAsteroidSpawn = 0;


	[Header("UI Elemetns")]
	public Canvas mainCanvas;
	public PopUpControl popUp;
	public Text asteroidCounter;
	public GameObject saveWindowPrefab;

	[Header("Borders")]
	public Transform borderTop;
	public Transform borderRight;
	public Transform borderBottom;
	public Transform borderLeft;


	private int asteroidsLeft = 0;
	private int stageNumber = 0;
	private bool nextStagePending = true;
	private playerInstantiate instScript;

	// Use this for initialization
	void Start () {
		instScript = GetComponent<playerInstantiate>();
		Screen.showCursor = false;
		Time.timeScale = 1;
		NextStage();
	}
	
	// Update is called once per frame
	void Update () {
		if(asteroidsLeft <= 0 && !nextStagePending){
			asteroidsLeft = 0;
			nextStagePending = true;
			popUp.SetCountdown(nextStageDelay,"Stage " + (stageNumber+1) + "\nin", NextStage);
		}
	}

	void onAsteroidDestroy(){
		asteroidsLeft--;
		UpdateAsteroidCounter();
		scoreManager.Add(scorePerAsteroid);
	}

	void onAsteroidSplit(GameObject[] asteroids){
		foreach(var asteroid in asteroids){
			asteroidsLeft++;
			asteroid.GetComponent<Asteroid>().onSplit+=onAsteroidSplit;
			asteroid.GetComponent<LifeControl>().onDestroy += onAsteroidDestroy;
		}
		UpdateAsteroidCounter();
	}

	void InstatiateAsteroids(int num){
		if(maxAsteroidSpawn > 0){
			num = Mathf.Min(num, maxAsteroidSpawn);
		}

		for(int i = 0; i<num;i++){

			Vector3 position = new Vector3();

			position.x = Random.Range(borderLeft.position.x, borderRight.position.x);
			position.y = Random.Range(borderBottom.position.y, borderTop.position.y);

			var asteroid = Instantiate(asteroidPrefab, position, Quaternion.identity) as GameObject;

			asteroid.rigidbody2D.AddForce(new Vector2(Random.Range(-50f,50f), Random.Range(-50f,50f)));

			asteroid.GetComponent<Asteroid>().onSplit+= onAsteroidSplit;
			asteroid.GetComponent<LifeControl>().onDestroy+=onAsteroidDestroy;
			asteroidsLeft++;
		}

		UpdateAsteroidCounter();
	}

	void NextStage(){

		if(stageNumber>0){
			scoreManager.Add(instScript.lifesLeft * scorePerLife);
		}

		stageNumber++;
		nextStagePending = false;
		InstatiateAsteroids(stageNumber);
	}

	void UpdateAsteroidCounter(){
		asteroidCounter.text = asteroidsLeft.ToString();
	}

	public void GameOver(){
		Screen.showCursor = true;
		Time.timeScale = 0;
		if (scoreManager.Score<0.01f) {
			PopUpControl.CountdownAction action = () => Application.LoadLevel ("MainMenu");
			popUp.SetCountdown (nextStageDelay, "Game Over!", action, false);
		}
		else{
			var saveWindow = Instantiate(saveWindowPrefab) as GameObject;
			saveWindow.transform.SetParent(mainCanvas.transform, false);
		}
	}
}
