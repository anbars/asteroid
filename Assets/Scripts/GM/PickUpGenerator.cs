﻿using UnityEngine;
using System.Collections;

public class PickUpGenerator : MonoBehaviour {


	public GameObject[] pickupPrefabs;

	[Range(0, 1f)]
	public float chance = 0.33f;

	void Update(){
	}
	
	public void Try(){
		if(Random.value < chance){
			int index = (int)(Random.value * 1000);
			index %= pickupPrefabs.Length;
			Instantiate(pickupPrefabs[index], transform.position, Quaternion.identity);
		}
	 }
}
