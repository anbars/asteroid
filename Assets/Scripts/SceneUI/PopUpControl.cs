﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopUpControl : MonoBehaviour {

	private Text text;

	public delegate void CountdownAction();

	private CountdownAction countdownAction;
	private bool countdownPending  = false;
	private float countdownTimer = 0f;
	private string countdownMessage;
	private bool showCountDown = true;

	// Use this for initialization
	void Start () {
		text = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if(countdownPending && countdownTimer > 0){
			countdownTimer -= Time.unscaledDeltaTime;
			string message = countdownMessage;
			if(showCountDown){
				message += "\n" + Mathf.Round(countdownTimer) + "...";
			}
			SetText(message);
		}
		else if(countdownPending){
			countdownPending = false;
			Hide();
			if (countdownAction != null) {
				countdownAction();
				countdownAction = null;
			}
		}
	}

	public void Show(){
		text.enabled = true;
	}


	public void Hide(){
		text.enabled = false;
	}

	public void SetText(string newText){
		text.text = newText;
	}

	public void SetCountdown (float time, string message, CountdownAction action, bool showTimer = true){
		countdownAction = action;
		countdownTimer = time;
		countdownMessage = message;
		countdownPending = true;
		showCountDown = showTimer;
		Show();
	}

	public void SetCountdown (float time, string message){
		SetCountdown(time, message, null);
	}
}
