﻿using UnityEngine;
using System.Collections;

public class ExitConfirm : MonoBehaviour {

	public void Confirm(){
		Application.Quit();
	}

	public void Cancel(){
		Destroy(gameObject);
	}
}
