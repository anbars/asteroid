﻿using UnityEngine;
using System.Collections;

public class ConfirmWindow : MonoBehaviour {

	public delegate void Action ();
	Action actionOnConfirm;
	Action actionOnCancel;


	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			Cancel();
		}
	}

	public void Confirm(){
		if(actionOnConfirm != null){
			actionOnConfirm();
		}
		else{
			Cancel();
		}
		Destroy(gameObject);
	}

	public void Cancel(){
		if(actionOnCancel != null){
			actionOnCancel();
		}
		Destroy(gameObject);
	}

	public void SetAction(Action onConfirm, Action onCancel = null){
		actionOnConfirm = onConfirm;
		actionOnCancel = onCancel;
	}
}
