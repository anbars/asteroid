﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CounterWithDelay : MonoBehaviour {

	[Range(0, 1f)]
	public float lerpRate;

	public Text counter;

	private float currentValue;
	private float targetValue;
	
	// Update is called once per frame
	void Update () {
		if(Mathf.Abs(currentValue - targetValue) > 0.01f){
			currentValue = Mathf.Lerp(currentValue, targetValue, lerpRate);
		}
		counter.text = Mathf.Round(currentValue).ToString();
	}

	public void SetValue(float newValue){
		targetValue = newValue;
	}

	public void Add(float delta){
		targetValue += delta;
	}
}
