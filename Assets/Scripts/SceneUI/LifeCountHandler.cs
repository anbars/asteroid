﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeCountHandler : MonoBehaviour {

	public Text text;
	public playerInstantiate instantiateScript;

	public LifeBarHandler lifeBarHandler;


	private LifeControl playerLifeControl;

	void Start(){
		instantiateScript.onInstatiate += onPlayerShipInstatiate;
	}


	void UpdateCounter(float delta, float current){
		text.text = Mathf.Round(current).ToString();
		lifeBarHandler.setLevel(playerLifeControl.LifeCurrent/playerLifeControl.lifeTotal);
	}

	void onPlayerShipInstatiate(GameObject newPlayerShip){
		playerLifeControl = newPlayerShip.GetComponent<LifeControl>();
		text.text = playerLifeControl.lifeTotal.ToString();
		lifeBarHandler.setLevel(0.99f);
		playerLifeControl.onLifeChange += UpdateCounter;
	}
}
