﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BombPanelHandler : MonoBehaviour {

	public  BigBangControl bombControl;

	public Text counter;

	private int bombs = 0;


	void Start(){
		bombs = bombControl.startBombs;
		UpdateCounter();
	}
	
	public void AddIcon(){
		bombs++;
		UpdateCounter();
	}
	
	public void RemoveIcon(){
		bombs--;
		UpdateCounter();
	}
	
	void UpdateCounter(){
		counter.text = bombs.ToString();
	}
}
