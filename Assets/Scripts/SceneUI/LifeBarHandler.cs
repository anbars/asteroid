﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class LifeBarHandler : MonoBehaviour {

	[Range(0,1f)]
	public float updateRate;

	public Image lifeBar;

	public Color fullColor;
	public Color emptyColor;


	private float currentLevel = 0;
	private float targetLevel = 0;

	void Start(){
		Vector3 newScale = lifeBar.rectTransform.localScale;
		newScale.y = 0;
		
		lifeBar.rectTransform.localScale = newScale ;
	}

	// Update is called once per frame
	void Update () {
		if(Mathf.Abs(currentLevel - targetLevel) > 0.001f){
			currentLevel = Mathf.Lerp(currentLevel, targetLevel, updateRate);
			lifeBar.color = Color.Lerp(emptyColor, fullColor, currentLevel);

			Vector3 newScale = lifeBar.rectTransform.localScale;
			newScale.y = currentLevel;
			
			lifeBar.rectTransform.localScale = newScale ;
		}
	}

	public void setLevel(float newLevel){
		targetLevel = newLevel;
	}
}
