﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LifePanelHandler : MonoBehaviour {


	public playerInstantiate instantiateScript;
	public Text counter;

	private int lifes = 0;


	void Start(){
		lifes = instantiateScript.startLifes;
		UpdateCounter();
	}

	public void AddIcon(){
		lifes++;
		UpdateCounter();
	}

	public void RemoveIcon(){
		lifes--;
		UpdateCounter();
	}

	void UpdateCounter(){
		counter.text = lifes.ToString();
	}







}
