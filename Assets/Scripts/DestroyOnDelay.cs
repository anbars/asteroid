﻿using UnityEngine;
using System.Collections;

public class DestroyOnDelay : MonoBehaviour {

	public float delayTime;

	// Use this for initialization
	void Start () {
		Invoke("DestroySelf", delayTime);
	}
	
	void DestroySelf(){
		Destroy(gameObject);
	}
}
