﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LifeControl))]
[RequireComponent(typeof(PickUpGenerator))]
public class Asteroid : MonoBehaviour {

	public float startSpeed = 20f;
	public float zeroSpeedThreshold = 1.5f;

	public GameObject explosionPrefab;


	private int size = 4;


	private AudioSource impactSound;
	private LifeControl lifeControl;
	private PickUpGenerator pickUpGenerator;

	public delegate void SplitEventHandler(GameObject[] asteroids);

	public event SplitEventHandler onSplit;

	// Use this for initialization
	void Start () {
		//rigidbody2D.AddForce(new Vector2 (Random.Range (-1, 1), Random.Range (-1, 1)) * startSpeed);
		rigidbody2D.AddTorque (5f);

		impactSound = GetComponent<AudioSource>();

		pickUpGenerator = GetComponent<PickUpGenerator>();

		lifeControl = GetComponent<LifeControl>();
		lifeControl.onDestroy+=Split;
		lifeControl.onDestroy += pickUpGenerator.Try;
		gameObject.collider2D.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		var velocityAdjustment = Vector2.zero;
		if(Mathf.Abs(rigidbody2D.velocity.x) < zeroSpeedThreshold){
			velocityAdjustment.x = Random.Range(-3f,3f);
		}
		if(Mathf.Abs(rigidbody2D.velocity.y) < zeroSpeedThreshold){
			velocityAdjustment.y = Random.Range(-3f,3f);
		}
		rigidbody2D.velocity += velocityAdjustment;
	}

	void OnCollisionEnter2D(Collision2D col){
		var other = col.gameObject;
		lifeControl.TakeDamage (5f);
		Vector2 forceDirection = (transform.position - other.transform.position);
		forceDirection.Normalize ();
		rigidbody2D.AddForce (forceDirection*200f);
		impactSound.Play();
	}



	void Split(){
		if (size>1) {
			Vector2 startPulse = new Vector2 (Random.Range (-1, 1) * 100f, Random.Range (-1, 1) * 100f);
			GameObject[] shades = new GameObject[2];
			for (int i=0; i<2; i++) {
				shades[i] = GameObject.Instantiate (gameObject, transform.position + Vector3.up * i, Quaternion.identity) as GameObject;
				shades[i].GetComponent<Asteroid> ().SetSize (size - 1);
				shades[i].rigidbody2D.AddForce (startPulse);
				startPulse *= -1;
			}
			if(onSplit != null){
				onSplit(shades);
			}
		}
	}

	private void SetSize(int newSize){
		Vector3 newScale = new Vector3 (newSize / 4f,newSize / 4f,newSize / 4f);
		transform.localScale = newScale;
		this.size = newSize;
	}
}
