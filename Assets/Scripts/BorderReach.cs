﻿using UnityEngine;
using System.Collections;

public class BorderReach : MonoBehaviour {


	void Update(){
		if(Mathf.Abs(transform.position.x)>100 || Mathf.Abs(transform.position.y)>20){
			Vector3 newPosition = new Vector3();
			newPosition.x = 5;
			newPosition.y = 5;
			transform.position = newPosition;
		}
		
	}


	void OnTriggerEnter2D(Collider2D other){


		if (other.tag == "VBorder") {
			Vector3 newPosition = transform.position;
			newPosition.y = -transform.position.y + Mathf.Sign(transform.position.y);
			transform.position = newPosition; 
		}
		if (other.tag == "HBorder") {
			Vector3 newPosition = transform.position;
			newPosition.x = -transform.position.x + Mathf.Sign(transform.position.x);
			transform.position = newPosition; 
		}
	}

}
